package br.com.treinamento.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.web.client.RestTemplate;

import br.com.treinamento.model.HeroResponse;
import br.com.treinamento.model.Response;

@Component
public class HeroClient {

	@Autowired
	private RestTemplate restTemplate;

	private static final String TS = "1";
	private static final  String PRIVATE_KEY = "006c6060dd10e074d0586771f853bd095e4ca950";
	private static final  String PUBLIC_KEY = "f062355d611f1dede3a0268767a0a19a";
	private static final String ENDPOINT = "http://gateway.marvel.com/v1/public/characters";
	
	public Response findAll() {
		return this.restTemplate.getForObject(getCompleteEndpoint(), Response.class);
	}

	private String getCompleteEndpoint() {
		String digestStr = TS + PRIVATE_KEY + PUBLIC_KEY;
		String completeEndpoint = String.format(ENDPOINT + "?ts=%s&apikey=%s&hash=%s", TS, PUBLIC_KEY,
				DigestUtils.md5DigestAsHex(digestStr.getBytes()));
		return completeEndpoint;
	}

}
