package br.com.treinamento.dojo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.model.Hero;
import br.com.treinamento.service.HeroService;

@RestController
public class HeroController {
	
	@Autowired
	private HeroService heroService;
	
	@RequestMapping(value = "/hero", method = RequestMethod.GET)
	public ResponseEntity<List<Hero>> findAll() {
		List<Hero> heroes = heroService.findAll();
		return new ResponseEntity<List<Hero>>(heroes, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/hero/{id}", method = RequestMethod.GET)
	public ResponseEntity<Hero> find(@PathVariable("id") Long id) {
		Hero hero = heroService.find(id);
		return new ResponseEntity<Hero>(hero, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/hero/", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody Hero hero) {
		heroService.create(hero);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/hero/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@PathVariable("id") Long id,  @RequestBody Hero hero) {
		heroService.update(hero);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/hero/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
		heroService.delete(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}


}
