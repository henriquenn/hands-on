package br.com.treinamento.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.treinamento.client.HeroClient;
import br.com.treinamento.model.Hero;

@Service
public class HeroService {

	@Autowired
	private HeroClient heroClient;

	public List<Hero> findAll() {
		return heroClient.findAll().getData().getResults();
	}

	public Hero find(Long id) {
		return null;
	}

	public void create(Hero hero) {

	}
	
	public void update(Hero hero) {

	}
	
	public void delete(Long id) {

	}
	
	
}
